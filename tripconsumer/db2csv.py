"""
Load Trip data from DataMint DB @ 192.168.1.95 and store in a local csv file
for using with Jupyter notebook.
"""

import pyodbc
import pandas as pd
import os

mid = 2  # metropian ID

try:
    conn = pyodbc.connect('DSN=metropia;UID=ali;PWD=ali1234')
except pyodbc.Error:
    print('Failed to connect to DataMint!')
    exit(-1)
else:
    cursor = conn.cursor()

    sql_stm = """
SELECT TripSummaryID, TripMetroCity, StartLatitude, StartLongitude, 
EndLatitude, EndLongitude, LocalStartTime, LocalTripEndTime, LocalWeekDay
FROM rptTripSummary WHERE MetropianID = {} AND StartLatitude IS NOT NULL AND userEarnedCredit > 0
""".format(mid)

    df = pd.read_sql_query(sql_stm, conn)

    cities = ['Tucson', 'Austin', 'ElPaso']
    df = df.loc[df['TripMetroCity'].isin(cities)]
    
    df.columns = ['tid', 'city', 'orig_lat', 'orig_lon', 'dest_lat', 'dest_lon',
                  'local_s_time', 'local_e_time', 'day_of_week']
    df['local_e_time'] = pd.to_datetime(df['local_e_time'])

    # get rid of macroseconds part
    df['local_e_time'] = df['local_e_time'].apply(lambda dt: dt.strftime("%Y-%m-%d %H:%M:%S"))

    # get the start date
    start_date = (str(((df[df['local_s_time'] == min(df['local_s_time'])])['local_s_time'].tolist())[0])).split(" ")[0].split("-")
    
    # get the end date
    end_date = (str(((df[df['local_e_time'] == max(df['local_e_time'])])['local_e_time'].tolist())[0])).split(" ")[0].split("-")
    
    # get all the generated files under certain path
    dirs = os.listdir('./userfile')

    # True -> generate file; False -> not generate file
    flag = True
    for file in dirs:
        info = file.split("_")
        if len(info) != 8:
            continue

        seq = (info[5], info[6], info[7].split(".")[0])
        file_end_date = "-".join(seq)
        
        # check current user's file has been generated or not
        if int(info[1]) == mid:
            if file_end_date < "-".join(end_date):
                flag = True
                path = './userfile/' + file
                os.remove(path)
            else:
                flag = False

    if flag:
        df.to_csv('./userfile/trip_{}_{}_{}_{}_{}_{}_{}.csv'.format(mid, start_date[0], start_date[1], start_date[2], end_date[0], end_date[1], end_date[2]), index=False)

    conn.close()